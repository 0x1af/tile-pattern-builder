

# Usage:

1. Create a config.yaml:

```
output:
  size: ['297cm','210cm']
  tile_size: 500 #pt
  offset: 200 #pt
  tile_gap: 50 #pt
  bg_color: "#ffffff"
  gap_color: "#505050"
tiles:
  x: 'img1.png'
  o: 'img2.png'
  a: 'img3.png'
  s: 'img4.png'
```

2. Create a cool tile pattern, where each character represents a tile (as defined in the config.yaml before):
```
xoa
xoas
oassxx
```

3. Run the python script. It does create the output files pattern.svg and pattern.pdf.
```
tile-pattern-builder.py pattern.txt
```

Enjoy!

# Known bugs:
* Only square tiles are supported so far =D
* Output files are created in pwd