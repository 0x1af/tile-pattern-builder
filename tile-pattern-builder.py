#!/usr/bin/env python3

# written by 0x1AF


import yaml
import svgwrite
import cairosvg
import os
import argparse

def parse_arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('patternfile')
	parser.add_argument('-c', '--config', default='config.yaml', dest="configfile")
	return parser.parse_args()

def parse_config_file(config_filename):
	with open(config_filename,'r') as config_file:
		return yaml.load(open('config.yaml','r'))

def parse_pattern_file(pattern_filename):
	pattern_array = []
	with open(pattern_filename) as pattern_file:
		for y, line in enumerate(pattern_file):
			pattern_array.append([])
			for x, tile in enumerate(line):
				if not tile.isspace():
					pattern_array[y].append(tile)

	return pattern_array

def generate_svg_drawing(config, pattern_array, file_name):
	
	output_size = config["output"]["size"]
	tile_size = config["output"]["tile_size"]
	tile_gap = config["output"]["tile_gap"]
	offset = config["output"]["offset"]
	bg_color = config["output"].get("bg_color", "#ffffff")
	gap_color = config["output"].get("gap_color", "#ffffff")
	tile_definitions = config['tiles']

	drawing = svgwrite.Drawing(file_name+'.svg', size=output_size, profile='tiny')

	# stolen from: https://stackoverflow.com/questions/22882247/python-svgwrite-module-background-color
	bg_color_rect = drawing.rect(insert=(0, 0), size=('100%', '100%'), rx=None, ry=None, fill=bg_color)
	drawing.add(bg_color_rect)

	max_row_len = len(max(pattern_array , key = len))
	tile_gap_color_rect_size = [max_row_len*(tile_size+tile_gap)+tile_gap , len(pattern_array) *(tile_size+tile_gap)+ tile_gap]
	tile_gap_color_rect = drawing.rect(
		insert=(offset, offset), size=tile_gap_color_rect_size, fill=gap_color
	)

	drawing.add(tile_gap_color_rect)


	for i, row in enumerate(pattern_array):
		for j, tile in enumerate(row):
			size = (tile_size, tile_size)
			tile_offset = offset + tile_gap
			position = (tile_offset+j*(tile_size+tile_gap), tile_offset+i*(tile_size+tile_gap))
			image_url = tile_definitions[tile]
			image = drawing.image(image_url, insert=position, size=size)
			drawing.add(image)

	return drawing

def save_svg_as_pdf(svg_drawing, file_name):
	cairosvg.svg2pdf(bytestring=svg_drawing.tostring(), write_to=file_name+'.pdf')


if __name__ == '__main__':
	arguments = parse_arguments()
	config = parse_config_file(arguments.configfile)
	pattern_array = parse_pattern_file(arguments.patternfile)

	file_basename = os.path.splitext(os.path.basename(arguments.patternfile))[0]
	svg_drawing = generate_svg_drawing(config, pattern_array, file_basename)
	svg_drawing.save()
	save_svg_as_pdf(svg_drawing, file_basename)
















